<?php

namespace App\Entity;

use App\Repository\SuscripcionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


#[ORM\Entity(repositoryClass: SuscripcionRepository::class)]
#[UniqueEntity(fields: 'email', errorPath: 'email', message:'Email ya registrado')]
class Suscripcion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Assert\Length(
     min: 2,
     max: 100,
     minMessage: 'Nombre demasiado corto, mínimo {{ limit }} caracteres',
     maxMessage: 'Nombre demasiado largo, máximo {{ limit }} caracteres',
     normalizer:'trim'
    )]
    #[Assert\Regex(
     pattern: '/[\d<>"\']/',
     match: false,
     message: 'Tu nombre solo puede tener letras',
    )]
    private ?string $nombre = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Email(message: 'Email inválido.')]
    private ?string $email = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }
}
