<?php

namespace App\Controller;

use App\Entity\Suscripcion;
use App\Form\SuscripcionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SuscripcionController extends AbstractController
{
    #[Route('/suscripcion', name: 'app_suscripcion')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $suscripcion = new Suscripcion();
        /** @var Form $form */
        $form = $this->createForm(SuscripcionType::class, $suscripcion);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($suscripcion);
            $entityManager->flush();

            $this->addFlash(
                'success',
                $suscripcion->getNombre().' Has quedado suscrito a las noticias con el email: '.$suscripcion->getEmail()
            );
            return $this->redirectToRoute('app_suscripcion_success');
        }

        return $this->render('suscripcion/index.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/suscripcion-success', name: 'app_suscripcion_success')]
    public function success(Request $request): Response
    {
        $session = $request->getSession();

        if(!$session->getFlashBag()->has('success')) {
            return $this->redirectToRoute('app_suscripcion');
        }

        return $this->render('suscripcion/success.html.twig', );
    }
}
