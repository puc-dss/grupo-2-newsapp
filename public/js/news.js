window.addEventListener('load', () => {
    const btnVerMasSelector = document.getElementById('ver-mas');
    const newsListSelector = document.getElementById('news-list');

    const API_KEY = newsListSelector.getAttribute('data-token');
    const url = `https://eventregistry.org/api/v1/article/getArticles`;
    const articles = [];

    const fetchData = async () => {
        const queryParams = {
            "action": "getArticles",
            "keyword": "artificial intelligence",
            "articlesPage": 1,
            "articlesCount": 200,
            "articlesSortBy": "date",
            "articlesSortByAsc": false,
            "articlesArticleBodyLen": 150,
            "resultType": "articles",
            "includeSourceDescription": true,
            "includeSourceLocation": true,
            "dataType": [
                "news",
                "pr"
            ],
            "lang": [
                "spa",
                "eng"
            ],
            "apiKey": API_KEY,
            "forceMaxDataTimeWindow": 31
        };

        const params = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(queryParams)
        };

        const response = await fetch(url, params);

        return await response.json();
    };

    const isValidUrl = urlString => {
        const urlElement = document.createElement('input');
        urlElement.type = 'url';
        urlElement.value = urlString;
        return urlElement.checkValidity();
    };

    const escapeHTML = str => str.replace(/[&<>'"]/g, tag => ({
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            "'": '&#39;',
            '"': '&quot;'
        }[tag] || tag)
    );

    const validateNews = newsData => {
        const validNews = {};

        if(newsData.url!==undefined && newsData.url!==null && isValidUrl(newsData.url)) {
            validNews.url = newsData.url;
        }

        if(newsData.image!==undefined && newsData.image!==null && isValidUrl(newsData.image)) {
            validNews.urlToImage = newsData.image;
        }

        if(newsData.title!==undefined && newsData.title!==null) {
            validNews.title = escapeHTML(newsData.title);
        }
        else {
            return false;
        }

        if(newsData.body!==undefined && newsData.body!==null) {
            validNews.description = escapeHTML(newsData.body);
        }
        else {
            return false;
        }

        if(newsData.dateTimePub!==undefined && newsData.dateTimePub!==null) {
            const fecha = new Date(newsData.dateTimePub).toLocaleDateString();
            if(fecha!=="Invalid Date") {
                validNews.publishedAt = fecha;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }

        return validNews;
    };

    const renderNews = (borrarContenidoPrevio = false) => {
        btnVerMasSelector.hidden = false;
        for (let i = 0; i < 10; i++) {
            if(articles.length) {
                const news = validateNews(articles.pop());
                if(news!==false) {
                    const imagen = news.urlToImage ? `<img src=${news.urlToImage} class="card-img-top" alt="${news.title}">` : '';
                    const urlNews = news.url ? `<div class="card-footer"><a href=${news.url} target="_blank">ver más</a></div>` : '';
                    const newsCardHtml = `<div class="card">
                            ${imagen}
                            <div class="card-body">
                                <h5 class="card-title">${news.title}</h5>
                                <p class="text-muted"">${news.publishedAt}</p>
                                <p class="card-text">${news.description}</p>
                            </div>
                            ${urlNews}
                        </div>`;

                    if(borrarContenidoPrevio && i===0) {
                        newsListSelector.innerHTML = newsCardHtml;
                    }
                    else {
                        newsListSelector.innerHTML = `${newsListSelector.innerHTML}${newsCardHtml}`;
                    }
                }
            }
            else {
                btnVerMasSelector.hidden = true;
            }
        }
    };

    fetchData()
        .then(data => {
            // console.log(data);
            if(data.articles!==undefined && data.articles.results.length) {
                articles.push(...data.articles.results);
                renderNews(true);
            }
            else {
                newsListSelector.innerHTML = `<div class="alert alert-warning">No hay noticias disponibles</div>`;
            }
        })
        .catch(err => {
            console.log(err);
            newsListSelector.innerHTML = `<div class="alert alert-warning">No se pudo obtener las noticias en este momento</div>`;
        });

    btnVerMasSelector.addEventListener('click', () => {
        renderNews();
    });
});